import React from 'react';
import { Route, Switch } from 'react-router-dom';
import routers from '../src/routesConfig';
import NoMatch from '../src/components/404';

function AppRouteComponent() {

  return (
    <Switch>
      {routers.map((route, index) => {
        const { path, component: RouteComponent, exact, fallback: FallbackComp } = route;
        return (
          <Route key={`${path}-${index}`} path={path} exact={exact || false}>
            <React.Suspense fallback={<>loading...</>}>
              <RouteComponent />
            </React.Suspense>
          </Route>
        );
      })}
      <Route component={NoMatch} />
    </Switch>
  );
}

export default AppRouteComponent;
