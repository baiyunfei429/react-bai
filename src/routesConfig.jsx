import React from 'react';
import { UserOutlined, UnlockOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';

const Index = React.lazy(() =>
  import(
    './pages/index.tsx'
  ),
);

const Form = React.lazy(() =>
  import(
    './pages/form.jsx'
  ),
);

const BeginWork = React.lazy(() =>
  import(
    './pages/begin-work.jsx'
  ),
);

const Modal = React.lazy(() =>
  import(
    './pages/modal/index.jsx'
  ),
);

const ModalStatic = React.lazy(() =>
  import(
    './pages/modalStatic.jsx'
  ),
);

const Log = React.lazy(() =>
  import(
    './pages/log.jsx'
  ),
);

const DiyRedux = React.lazy(() =>
  import(
    './pages/diyRedux.jsx'
  ),
);

const routeMenus = [
  {
    path: "/",
    exact: true,
    component: Index,
    key: '/',
    icon: (<UnlockOutlined />),
    label: <Link to={{ pathname: '/' }}>home</Link>,
    fallback: null,
  },
  { 
    path: "/form",
    exact: false,
    component: Form,
    key: '/form',
    icon: (<UnlockOutlined />),
    label: <Link to={{ pathname: '/form' }}>form</Link>,
    fallback: null,
  },
  { 
    path: "/begin-work",
    exact: false,
    component: BeginWork,
    key: '/begin-work',
    icon: (<UnlockOutlined />),
    label: <Link to={{ pathname: '/begin-work' }}>begin-work</Link>,
    fallback: null,
  },
  { 
    path: "/modal",
    exact: false,
    component: Modal,
    key: '/modal',
    icon: (<UnlockOutlined />),
    label: <Link to={{ pathname: '/modal' }}>modal</Link>,
    fallback: null,
  },
  { 
    path: "/modalStatic",
    exact: false,
    component: ModalStatic,
    key: '/modalStatic',
    icon: (<UnlockOutlined />),
    label: <Link to={{ pathname: '/modalStatic' }}>modalStatic</Link>,
    fallback: null,
  },
  { 
    path: "/log",
    exact: false,
    component: Log,
    key: '/log',
    icon: (<UserOutlined />),
    label: <Link to={{ pathname: '/log' }}>log</Link>,
    fallback: null,
  },
  { 
    path: "/diyRedux",
    exact: false,
    component: DiyRedux,
    key: '/diyRedux',
    icon: (<UserOutlined />),
    label: <Link to={{ pathname: '/diyRedux' }}>diyRedux</Link>,
    fallback: null,
  },
];

export default routeMenus
