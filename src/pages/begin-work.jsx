import React from "react";

/* 子组件2 */
function Child2(){
  return <div>子组件 2</div>
}
/* 子组件1 */
function Child1(){
  const [ num , setNumber ] = React.useState(0)
  return <div>
      子组件 {num}
      <button onClick={() => setNumber(num+1)} >按钮1</button>
   </div>
}
/* 父组件 */
export default function Index(){
  const [ num , setNumber ] = React.useState(0)
  return <div>
      <p>父组件 {num} </p>
      <Child1 />
      <Child2 />
      <button onClick={()=> setNumber(num+1)} >按钮2</button>
  </div>
}