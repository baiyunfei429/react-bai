import { Alert, Divider, Button, Form, Input } from "bai-design-react";
// import { Alert, Divider, Input, Button } from "bai-design-react";
import "bai-design-react/esm/alert/style";
import "bai-design-react/esm/divider/style";
import "bai-design-react/esm/button/style";
import { useEffect, useRef } from "react";

const FormItem = Form.FormItem;

export default function HomePage() {
  const form = useRef(null);
  useEffect(() => {
    console.log(form.current, "form.current");
  }, []);
  const handleClick = () => {
    form?.current?.submit((res: any) => {
      console.log(res);
    });
  };
  const handleGetValue = () => {
    console.log(
      form.current.getFieldsValue(),
      "form.current.getFieldsValue() "
    );
  };
  return (
    <div>
      <h2> Welcome to Home!</h2>
      <Alert kind="info">这是一条警告提示baiyunfei</Alert>
      <Divider />
      {/* <Button>姓名：</Button><Input placeholder="请输入姓名" /> */}
      <Divider />
      <Alert kind="warning">这是一条警告提示</Alert>
      <Divider />
      <p>
        <Alert kind="negative">这是另一条警告提示</Alert>
      </p>
      <>
        Baiyunfei is handsome.skr~~
        <Divider />
        This is a react UI components.
        <Divider />
        yoyo, qiekenao
      </>
      <div>
        白云飞
        <Divider type="vertical" />
        深色模式
        <Divider type="vertical" />
        英文
      </div>
      <Form initialValues={{ author: "baiyunfei429" }} ref={form}>
        <FormItem
          label="请输入名称"
          labelWidth={150}
          name="name"
          required
          rules={{
            rule: /^[a-zA-Z0-9_\u4e00-\u9fa5]{4,32}$/,
            message: "名称仅支持中文、英文字母、数字和下划线，长度限制4~32个字",
          }}
          validateTrigger="onBlur"
        >
          <Input placeholder="小册名称" />
        </FormItem>
        <FormItem
          label="作者"
          labelWidth={150}
          name="author"
          required
          validateTrigger="onBlur"
        >
          <Input placeholder="请输入作者" />
        </FormItem>
        <FormItem
          label="邮箱"
          labelWidth={150}
          name="email"
          rules={{
            rule: /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/,
            message: "邮箱格式错误！",
          }}
          validateTrigger="onBlur"
        >
          <Input placeholder="请输入邮箱" />
        </FormItem>
        <FormItem
          label="手机"
          labelWidth={150}
          name="phone"
          rules={{ rule: /^1[3-9]\d{9}$/, message: "手机格式错误！" }}
          validateTrigger="onBlur"
        >
          <Input placeholder="请输入邮箱" />
        </FormItem>
        <FormItem
          label="简介"
          labelWidth={150}
          name="des"
          rules={{
            rule: (value = "") => value?.length < 5,
            message: "简介不超过五个字符",
          }}
          validateTrigger="onBlur"
        >
          <Input placeholder="输入简介" />
        </FormItem>
        <div style={{ marginTop: "20px" }}>
          <Button className="btn" onClick={handleClick} type="primary" loading>
            提交
          </Button>
          <Button className="btn" type="default" htmlType="reset">
            重置
          </Button>
        </div>
      </Form>
      <div style={{ marginTop: "20px" }}>
        <span>验证表单功能：</span>
        <Button className="btn" onClick={handleGetValue} type="primary">
          获取表单数层
        </Button>
        <Button
          className="btn"
          onClick={() =>
            form?.current?.validateFields((res: any) => {
              console.log("是否通过验证：", res);
            })
          }
          danger
        >
          动态验证表单
        </Button>
      </div>
    </div>
  );
}
