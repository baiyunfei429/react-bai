import Modal from '../components/Modal'
export default function Index(){
  const handleClick =() => {
      Modal.show({
          content:<p>确定购买《React》吗</p>,
          title:'《React》',
          onOk:()=>console.log('点击确定'),
          onCancel:()=>console.log('点击取消'),
          onClose:()=> Modal.hidden()
      })
  }
  return <div>
      <button onClick={() => handleClick()} >静态方式调用，显示modal</button>
  </div>
}