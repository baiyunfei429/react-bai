import './App.css'
import { BrowserRouter } from 'react-router-dom';
import AppRouteComponent from './AppRouteComponent';
import Layout from '../src/components/layouts';

export default function App() {
  return (
    <BrowserRouter basename="/">
      <Layout>
        <AppRouteComponent />
      </Layout>
    </BrowserRouter>
  );
}