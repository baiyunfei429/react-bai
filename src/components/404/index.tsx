import './index.less';

export default function Index() {
  return (
    <div className='not-find-wrap'>
      <h2>404</h2>
      <div className={'not-find-guide'}>
        {" The page you're trying to visit doesn't exist. Go back to the "}
        <a href="/">home page</a>
      </div>
    </div>
  );
}
