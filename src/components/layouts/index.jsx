import './index.less';
import routers from '../../routesConfig';
import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import { Menu, Layout } from 'antd';

const { Sider, Content } = Layout;

const contentStyle = {
  height: '100vh',
  overflowY: 'scroll',
};
const siderStyle = {
  width: '100%',
  minHeight: '100vh',
};

export default function Index({ children }) {
  const { pathname } = useLocation();
  const [current, setCurrent] = useState(pathname);
  const menuItems = React.useMemo(() => {
    return routers.map(route => {
      const { path, icon, label, key } = route;
      return { path, icon, label, key };
    });
  }, [routers]);
  const onClick = e => {
    console.log('click ', e);
    setCurrent(e.key);
  };
  return (
    <Layout>
      <Sider style={siderStyle}>
        <Menu theme="dark" onClick={onClick} selectedKeys={[current]} mode="vertical" items={menuItems} />
      </Sider>
      <Layout>
        <Content style={contentStyle}>
          <div className="content">{children}</div>
        </Content>
      </Layout>
    </Layout>
  );
}
